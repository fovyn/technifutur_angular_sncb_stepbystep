import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BRailStation} from '../models/b-rail.model';
import {environment} from '../../environments/environment';
import {BRailService} from '../services/b-rail.service';
import {filter, map} from 'rxjs/operators';
import {from} from 'rxjs';

@Component({
  selector: 'app-station-list',
  templateUrl: './station-list.component.html',
  styleUrls: ['./station-list.component.scss']
})
export class StationListComponent implements OnInit {
  @Input('dataSource') data: Array<BRailStation> = [];
  @Output('selectEvent') selectEvent: EventEmitter<BRailStation> = new EventEmitter<BRailStation>();

  constructor(private bRailService: BRailService) { }

  ngOnInit(): void {
    if (!environment.production) {
      this.bRailService.Stations$.subscribe(data => this.data = data);
    }
  }

  selectStationAction(id: string) {
    this.selectEvent.emit(this.data.find(s => s.id === id));
  }
}
