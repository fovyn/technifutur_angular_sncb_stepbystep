import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StationListComponent } from './station-list/station-list.component';
import { StationDisplayComponent } from './station-display/station-display.component';
import { StationCorrespondanceComponent } from './station-correspondance/station-correspondance.component';
import { ConvertToDatePipe } from './pipes/convert-to-date.pipe';
import {DatePipe} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    StationListComponent,
    StationDisplayComponent,
    StationCorrespondanceComponent,
    ConvertToDatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
