export interface BRailStation {
  'id': string;
  '@id': string;
  'locationX': number;
  'locationY': number;
  'standardname': string;
  'name': string;
}

export interface Correspondance {
  version: string;
  timestamp: number;
  connection?: (ConnectionEntity)[] | null;
}
export interface ConnectionEntity {
  id: number;
  departure: Departure;
  arrival: Arrival;
  duration: number;
  alerts: Alerts;
  vias: Vias;
}
export interface Departure {
  delay: number;
  station: string;
  stationinfo: BRailStation;
  time: number;
  vehicle: string;
  vehicleinfo: Vehicleinfo;
  platform: number;
  platforminfo: Platforminfo;
  left: number;
  canceled: number;
  direction: Direction;
  stops: Stops;
  alerts: Alerts;
  walking: number;
  departureConnection: string;
}

export interface Vehicleinfo {
  name: string;
  shortname: string;
  '@id': string;
}
export interface Platforminfo {
  name: string;
  normal: string;
}
export interface Direction {
  name: string;
}
export interface Stops {
  number: number;
  stop?: (StopEntity)[] | null;
}
export interface StopEntity {
  id: number;
  station: string;
  stationinfo: BRailStation;
  time: number;
  delay: number;
  canceled: number;
  departureDelay: number;
  departureCanceled: number;
  scheduledDepartureTime: number;
  arrivalDelay: number;
  arrivalCanceled: number;
  isExtraStop: number;
  scheduledArrivalTime: number;
  departureConnection: string;
}
export interface Alerts {
  number: number;
  alert?: (AlertEntity)[] | null;
}
export interface AlertEntity {
  id: number;
  header: string;
  lead: string;
  link: string;
  startTime: number;
  endTime: number;
}
export interface Arrival {
  delay: number;
  station: string;
  stationinfo: BRailStation;
  time: number;
  vehicle: string;
  vehicleinfo: Vehicleinfo;
  platform: number;
  platforminfo: Platforminfo;
  arrived: number;
  canceled: number;
  walking: number;
  direction: Direction;
}
export interface Vias {
  number: number;
  via?: (ViaEntity)[] | null;
}
export interface ViaEntity {
  id: string;
  arrival: Arrival1;
  departure: Departure1;
  timeBetween: number;
  station: string;
  stationinfo: BRailStation;
  vehicle: string;
  direction: Direction;
}
export interface Arrival1 {
  time: number;
  platform: number;
  platforminfo: Platforminfo;
  arrived: number;
  delay: number;
  canceled: number;
  vehicle: string;
  walking: number;
  direction: Direction;
}
export interface Departure1 {
  time: number;
  platform: number;
  platforminfo: Platforminfo;
  left: number;
  delay: number;
  canceled: number;
  departureConnection: string;
  vehicle: string;
  walking: number;
  alerts: Alerts;
  direction: Direction;
  stops: Stops;
}

export interface Departure {
  version: string;
  timestamp: number;
  station: string;
  stationinfo: Stationinfo;
  departures: Departures;
}
export interface Stationinfo {
  id: string;
  '@id': string;
  locationX: number;
  locationY: number;
  standardname: string;
  name: string;
}
export interface Departures {
  number: number;
  departure?: (DepartureEntity)[] | null;
}
export interface DepartureEntity {
  id: number;
  delay: number;
  station: string;
  stationinfo: Stationinfo;
  time: number;
  vehicle: string;
  vehicleinfo: Vehicleinfo;
  platform: number;
  platforminfo: Platforminfo;
  canceled: number;
  left: number;
  departureConnection: string;
  occupancy: Occupancy;
}
export interface Vehicleinfo {
  name: string;
  shortname: string;
  '@id': string;
}
export interface Platforminfo {
  name: string;
  normal: string;
}
export interface Occupancy {
  '@id': string;
  name: string;
}

export interface CorrespondanceData {station: string; time: number; }
