import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {BRailStation, ConnectionEntity, Correspondance, CorrespondanceData, Departure} from '../models/b-rail.model';
import {environment} from '../../environments/environment';
import {filter, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BRailService {
  subject: BehaviorSubject<number[]> = new BehaviorSubject<number[]>([]);
  constructor(private httpClient: HttpClient) {
    this.subject.next([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  }

  get Stations$(): Observable<BRailStation[]> {
    return this.httpClient.get<any>(`${environment.api_url}/stations/?format=json`)
        .pipe(map((data: {station: Array<BRailStation>}) => data.station));
  }

  getConnection$(stationId: string): Observable<{departure: CorrespondanceData[]}> {
    return this.httpClient.get<{departures: {departure: CorrespondanceData[]}}>(`${environment.api_url}/liveboard/?id=${stationId}&format=json`)
      .pipe(map(data => data.departures));
  }
}
