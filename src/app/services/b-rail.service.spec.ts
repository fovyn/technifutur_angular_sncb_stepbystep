import { TestBed } from '@angular/core/testing';

import { BRailService } from './b-rail.service';

describe('BRailService', () => {
  let service: BRailService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BRailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
