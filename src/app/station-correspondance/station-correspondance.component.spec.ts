import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationCorrespondanceComponent } from './station-correspondance.component';

describe('StationCorrespondanceComponent', () => {
  let component: StationCorrespondanceComponent;
  let fixture: ComponentFixture<StationCorrespondanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationCorrespondanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationCorrespondanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
