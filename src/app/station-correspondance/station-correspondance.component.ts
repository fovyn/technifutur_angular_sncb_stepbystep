import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BRailStation, ConnectionEntity} from '../models/b-rail.model';
import {environment} from '../../environments/environment';
import {BRailService} from '../services/b-rail.service';

@Component({
  selector: 'app-station-correspondance',
  templateUrl: './station-correspondance.component.html',
  styleUrls: ['./station-correspondance.component.scss']
})
export class StationCorrespondanceComponent implements OnInit, OnChanges {
  @Input('departureStation') station: BRailStation;
  connections: Array<{station: string, time: number}> = [];

  constructor(private bRailService: BRailService) { }

  ngOnInit(): void {

  }


  ngOnChanges({station}: SimpleChanges): void {
    if (station && station.currentValue != null) {
      this.bRailService.getConnection$(station.currentValue.id).subscribe(data => this.connections.push(...data.departure));
    }
  }

}
