import { Component } from '@angular/core';
import {BRailStation} from './models/b-rail.model';
import {$e} from 'codelyzer/angular/styles/chars';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'TechnifuturSncbStepByStep';
  selectedStation: BRailStation = null;

  selectAction($event: BRailStation) {
    this.selectedStation = $event;
  }
}
