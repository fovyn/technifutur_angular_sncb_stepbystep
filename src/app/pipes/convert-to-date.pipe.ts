import { Pipe, PipeTransform } from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
  name: 'convertToDate',
})
export class ConvertToDatePipe implements PipeTransform {

  constructor(private datePipe: DatePipe) {}

  transform(value: number, format: string): string {
    const date = new Date(value * 1000);

    return this.datePipe.transform(date, format).replace(':', ' h ');
  }

}
