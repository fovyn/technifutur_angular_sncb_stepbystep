import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BRailStation} from '../models/b-rail.model';
import {BRailService} from '../services/b-rail.service';

@Component({
  selector: 'app-station-display',
  templateUrl: './station-display.component.html',
  styleUrls: ['./station-display.component.scss']
})
export class StationDisplayComponent implements OnInit, OnChanges {
  @Input('station') item: BRailStation = null;

  constructor(private bRailService: BRailService) { }

  ngOnInit(): void {

  }

  ngOnChanges({item}: SimpleChanges): void {
    if (item && item.currentValue != null) {
      this.bRailService.getConnection$(item.currentValue.id).subscribe(data => console.log(data));
    }
  }
}
