import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationDisplayComponent } from './station-display.component';

describe('StationDisplayComponent', () => {
  let component: StationDisplayComponent;
  let fixture: ComponentFixture<StationDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
